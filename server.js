require('dotenv').config()
const express = require('express');
const router = require('./routes/router.js');
const app = express();
const port = process.env.PORT;
const host = process.env.HOST;
const bodyParser = require('body-parser');

app.listen(port, host, function () {
    console.log(`Listening on http://${host}:${port}`)
});

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/v1', router)