module.exports = [
  {
      "stat_name": "Tiempo de entrega",
      "stat_key": "delivery_time",
      "stat_unit": "Días",
      "stat_comparison": ">",
      "stat_threshold": 5,
      "unapproved_message": "No estás entregando tus productos dentro de los primeros 5 días",
      "approved_message": "Estás entregando tus productos dentro de los primeros 5 días"
  },
  {
      "stat_name": "Productos entregados",
      "stat_key": "delivered_orders",
      "stat_unit": "%",
      "stat_comparison": "<",
      "stat_threshold": 75,
      "unapproved_message": "Tienes menos de 75% de pedidos entregados",
      "approved_message": "Tienes más de 75% de pedidos entregados"
  },
  {
      "stat_name": "Ventas con reclamos",
      "stat_key": "claims_by_orders",
      "stat_unit": "%",
      "stat_comparison": ">",
      "stat_threshold": 10,
      "unapproved_message": "Tienes más de 10% de ventas con reclamos",
      "approved_message": "Tienes menos de 10% de ventas con reclamos"
  },
  {
      "stat_name": "Mensajes respondidos",
      "stat_key": "answered_messages",
      "stat_unit": "%",
      "stat_comparison": "<",
      "stat_threshold": 80,
      "unapproved_message": "Tienes menos de 80% de mensajes respondidos",
      "approved_message": "Tienes más de 80% de mensajes respondidos"
  },
  {
      "stat_name": "Tiempo de respuesta",
      "stat_key": "response_time",
      "stat_unit": "Horas",
      "stat_comparison": ">",
      "stat_threshold": 2,
      "unapproved_message": "No estás respondiendo los mensajes dentro de las primeras 2 horas",
      "approved_message": "Estás respondiendo los mensajes dentro de las primeras 2 horas"
  },
  {
      "stat_name": "Mínimo de ventas para la medición",
      "stat_key": "minimum_sales",
      "stat_unit": "Ventas",
      "stat_comparison": ">",
      "stat_threshold": 25,
      "unapproved_message": "No cuentas con el mínimo de 25 ventas para la medición",
      "approved_message": "Cuentas con el mínimo de 25 ventas para la medición"
  }
]
