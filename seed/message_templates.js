module.exports = {
  minimum_sales: {
    unapproved_message: "No cuentas con el mínimo de {stat_threshold} ventas para la medición",
    approved_message: "Cuentas con el mínimo de {stat_threshold} ventas para la medición"
  },
  delivery_time: {
    unapproved_message: "No estás entregando tus productos dentro de los primeros {stat_threshold} días",
    approved_message: "Estás entregando tus productos dentro de los primeros {stat_threshold} días"
  },
  response_time: {
    unapproved_message: "No estás respondiendo los mensajes dentro de las primeras {stat_threshold} horas",
    approved_message: "Estás respondiendo los mensajes dentro de las primeras {stat_threshold} horas"
  },
  delivered_orders: {
    unapproved_message: "Tienes menos de {stat_threshold}% de pedidos entregados",
    approved_message: "Tienes más de {stat_threshold}% de pedidos entregados"
  },
  claims_by_orders: {
    unapproved_message: "Tienes más de {stat_threshold}% de ventas con reclamos",
    approved_message: "Tienes menos de {stat_threshold}% de ventas con reclamos"
  },
  answered_messages: {
    unapproved_message: "Tienes menos de {stat_threshold}% de mensajes respondidos",
    approved_message: "Tienes más de {stat_threshold}% de mensajes respondidos"
  }
};
