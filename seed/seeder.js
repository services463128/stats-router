const db = require('../repositories/db')
const { v4: uuidv4 } = require('uuid');

exports.run = async (items, table) => {
  await clearItems(table);

  return await Promise.all(items.map(async (item) => {
    const params = {
      TableName: table,
      Item: item
    };
    const now = new Date().toISOString();
    params.Item.id = uuidv4();
    params.Item.created_at = now;
    params.Item.updated_at = now;    

    await db.put(params).promise();
  }));
}

clearItems = async (table) => {
  const items = (await db.scan({ TableName: table }).promise()).Items;
  await Promise.all(items.map(async (item) => {
    const params = {
        TableName: table,
        Key: { "id": item.id }
    };
    await db.delete(params).promise();
  }));
}