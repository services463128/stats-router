module.exports = [
  {
    "feature_active": true,
    "feature_approved_color": "green",
    "feature_description": "Insignia de Vendedor Destacado",
    "feature_name": "badge"
  },
  {
    "feature_active": true,
    "feature_approved_color": "green",
    "feature_description": "Presencia en categoria vendedores destacados",
    "feature_name": "featuredSellersCategory"
  },
  {
    "feature_active": true,
    "feature_approved_color": "yellow",
    "feature_description": "Productos visibles en resultados de búsqueda",
    "feature_name": "aperSearchCloud"
  }
];