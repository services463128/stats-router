<h3 align="center">Aperstats Router</h3>

---

<p align="center"> Api simple para consumir aperstats desde aws - prestashop
    <br> 
</p>

## 🏁 Getting Started <a name = "getting_started"></a>

Pasos para instalar localmente

### Installing

Copiar el .env

```
cp .env.example .env
```

Editar la url de dynamodb_endpoint y poner la iplocal

Levantar la base de dynamo

```
docker-compose -f docker-compose.dev.yml up -d
```

Admin dynamodb

```
http://0.0.0.0:8001/
```

Crear las tablas

```
./run.sh --vola-db
```

Cargar los seeds

```
curl http://0.0.0.0:3003/api/v1/seed-statType
```

```
curl http://0.0.0.0:3003/api/v1/seed-features
```

Ejecutar microservicio

```
yarn serve --dev
```

Instalar módulo aperstats

- Clonar módulo desde el repositorio
- Ejecutar docker install en el directorio del módulo
- Cambiar al directorio vue y ejecutar yarn install
- Ejecutar yarn build
- Desde el backoffice ir a la configuración del modulo e ingresar el endpoint http://[tu_ip]:3003/api/v1

Instalar lambdas localmente

- Clonar cada lambda desde el repositorio respectivo dentro de entornos-locales/ms
- Dentro del directorio de cada lambda ejecutar

```
cp .env.example .env
```

- Abrir y setear cada .env (la explicación está disponible en los comentarios de cada .env)
- Ejecutar:

```
npm install
```

- Verificar en el código de los repositorios que se encuentre descomentado require('dotenv').config(); (en aws no se requiere pero para trabajar en local sí)
- Correr lambdas, de a una, mediante

```
npm test
```

Listo!

En este punto, todo el circuito de aperstats debería estar funcionando. Se puede verificar yendo a:
Desde 1.7: Vender-->AperStats
Desde 1.6: Estadísticas-->AperStats
