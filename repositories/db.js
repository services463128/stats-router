const AWS = require("aws-sdk");

const config = {
  region: process.env.DYNAMODB_REGION,
}

if(process.env.NODE_ENV === 'dev'){
  config.endpoint = process.env.DYNAMODB_ENDPOINT,
  config.credentials = {
    accessKeyId: process.env.DYNAMODB_ACCESS_KEY_ID,
    secretAccessKey: process.env.DYNAMODB_SECRET_ACCESS_KEY
  }
}

 AWS.config.update(config)

module.exports = new AWS.DynamoDB.DocumentClient();