const db = require('./db');
const globals = require('./globalsRepository')
const detailsTable = process.env.TABLE_DETAILS;

exports.todays = async (idSupplier) => {  
  if (idSupplier){
    return await getSupplierDetails(idSupplier)
  }else{
    const suppliers = (await globals.all()).Items;
    const details = []
    await Promise.all(suppliers.map(async supplier => {
      const supplierDetails = await getSupplierDetails(supplier['id_supplier'])
      if(supplierDetails.length > 0) {
        details.push(supplierDetails[0])
      }
    }))
    
    return details
  }
}

getSupplierDetails = async (idSupplier) => {
  const todaysDetails = await getSupplierDetailsByDate(idSupplier, getDate())
  
  return todaysDetails.length > 0 ? todaysDetails : await getYesterdaysDetails(idSupplier)
}

getSupplierDetailsByDate = async (idSupplier, date) => {
  const params = { TableName: detailsTable };
  params.KeyConditionExpression = "id_supplier = :id_supplier and created_at = :created_at";
  params.ExpressionAttributeValues = {
    ":id_supplier": idSupplier,
    ":created_at": date
  };

  return (await db.query(params).promise()).Items;
}

getYesterdaysDetails = async (idSupplier) => {
  return await getSupplierDetailsByDate(idSupplier, getDate(1))
}

getDate = (minusDays = 0) => {
  const d = new Date();
  d.setDate(d.getDate() - minusDays)
  d.setUTCHours(0, 0, 0, 0);
  return d.toISOString();
}
