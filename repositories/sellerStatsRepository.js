const db = require('./db');
const sellerStatsTable = process.env.TABLE_SELLER_GENERAL_STATS;

exports.get = async (idSupplier) => {
  
  const params = {
    TableName: sellerStatsTable,
    KeyConditionExpression: "id_supplier = :id_supplier",
    ExpressionAttributeValues: { ":id_supplier": idSupplier }
  };
  
  return db.query(params).promise();
}