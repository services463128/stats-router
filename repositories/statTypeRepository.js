const db = require('./db');
const statTypeTable = process.env.TABLE_STATTYPE;

exports.all = async () => {
  return await db.scan({ TableName: statTypeTable }).promise();
}

exports.setStatTypes = async (thresholds) => {
  const templates = require('../seed/message_templates');
  const actualThresholds = (await this.all()).Items;
  
  return await Promise.all(
    thresholds.map(async threshold => {
      const actualThreshold = actualThresholds.find(t => t.id === threshold.id);
      if(!actualThreshold) { throw(Error('statType not found')); }

      const key = actualThreshold.stat_key
      const value = threshold.stat_threshold
      
      const params = {
        TableName: statTypeTable,
        Key: { id: threshold.id },
        UpdateExpression: `set stat_threshold = :threshold,
          updated_at = :dateUpdated,
          approved_message = :approvedMessage,
          unapproved_message = :unapprovedMessage`,
        ExpressionAttributeValues: {
          ':threshold': value,
          ':approvedMessage': getMessage(templates[key], value, 'approved'),
          ':unapprovedMessage': getMessage(templates[key], value, 'unapproved'),
          ':dateUpdated': new Date().toISOString()
        },
      };
      
      await db.update(params).promise();
    })
  );
}

const getMessage = (template, value, type) => {
  return template ? template[type+'_message'].replace("{stat_threshold}", value) : null;
}