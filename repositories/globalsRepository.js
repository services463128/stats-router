const db = require('./db');
const globalTable = process.env.TABLE_GLOBAL;

exports.all = async () => {
  return await db.scan({ TableName: globalTable }).promise();
}

exports.setGlobals = async (globals) => {
  const actual = await this.all();

  return await Promise.all(
    globals.map(async global => {
      const actualGlobal = actual.Items.find(g => g.id_supplier === global.id_supplier)
      if(!actualGlobal) { throw new Error('global not found') }

      const params = {
        TableName: globalTable,
        Key: { id_supplier: global.id_supplier },
        UpdateExpression: 'set active_features = :active_features',
        ExpressionAttributeValues: {
          ':active_features': global.active_features
        },
      };
     
     await db.update(params).promise();
    })
  );
}