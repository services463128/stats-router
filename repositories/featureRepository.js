const db = require('./db');
const featuresTable = process.env.TABLE_FEATURES;

exports.all = async () => {
  return await db.scan({ TableName: featuresTable }).promise();
}

exports.getFeatures = async (featureName) => {
  const params = { TableName: featuresTable };
  if (featureName){
    params.IndexName = "feature_name-index";
    params.KeyConditionExpression = "feature_name = :featureName";
    params.ExpressionAttributeValues = { ":featureName": featureName };
    return (await db.query(params).promise()).Items;
  } else {
    return (await this.all()).Items;
  }
}

get = async (id) => {
  const params = {
    TableName: featuresTable,
    KeyConditionExpression: "id = :id",
    ExpressionAttributeValues: { ":id": id }
  };

  return await db.query(params).promise();
}

exports.setFeatures = async (features) => {
  const actual = await this.all();

  return await Promise.all(
    features.map(async feature => {
      const actualFeature = actual.Items.find(f => f.id === feature.id)
      if(!actualFeature) { throw new Error('feature not found') }

      const params = {
        TableName: featuresTable,
        Key: { id: feature.id },
        UpdateExpression: 'set feature_active = :feature_active, updated_at= :dateUpdated',
        ExpressionAttributeValues: {
          ':feature_active': feature.feature_active,
          ':dateUpdated': new Date().toISOString()
        },
      };
      
      await db.update(params).promise();
    })
  );
}
