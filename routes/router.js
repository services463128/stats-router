const express = require('express');
const router = express.Router();

const featuresController = require('../controllers/featuresController');
const seederController = require('../controllers/seederController');
const statTypeController = require('../controllers/statTypeController');
const statsController = require('../controllers/statsController');
const globalsController = require('../controllers/globalsController');
const sellerStatsController = require('../controllers/sellerStatsController');
const processController = require('../controllers/processController');

router.get('/seed-features', seederController.features);
router.get('/seed-statType', seederController.statType);

router.get('/features', featuresController.get);
router.get('/features?:feature_name', featuresController.get);
router.post('/features', featuresController.set);

router.get('/statType', statTypeController.all);
router.post('/statType', statTypeController.set);

router.get('/stats', statsController.todays);
router.get('/stats?:id_supplier', statsController.todays);

router.get('/globals', globalsController.all)
router.post('/globals', globalsController.set)

router.get('/seller-stats?:id_supplier', sellerStatsController.get)

router.get('/process-details', processController.details)
router.get('/process-featured-sellers', processController.featuredSellers)

module.exports = router;