const repository = require('../repositories/featureRepository');

exports.get = async (req, res) => {
  try {
    const features = await repository.getFeatures(req.query.feature_name );
    res.send(features);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
};

exports.set = async (req, res) => {
  try {
    await repository.setFeatures(req.body);
    res.sendStatus(200);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
}