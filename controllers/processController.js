const AWS = require('aws-sdk');
const config = {
  region: process.env.DYNAMODB_REGION
}
AWS.config.update(config)

exports.details = async (req, res) => {
  const lambda = new AWS.Lambda();

  const params = {
    FunctionName: process.env.LAMBDA_DETAILS_CRON,
    InvocationType: 'RequestResponse',
    LogType: 'Tail'
  };

  lambda.invoke(params, function(err, data) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.send(data.Payload);
    }
  });
};

exports.featuredSellers = async (req, res) => {
  const lambda = new AWS.Lambda();

  const params = {
    FunctionName: process.env.LAMBDA_FEATURED_SELLERS_CRON,
    InvocationType: 'RequestResponse',
    LogType: 'Tail'
  };

  lambda.invoke(params, function(err, data) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.send(data.Payload);
    }
  });
};