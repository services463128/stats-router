const repository = require('../repositories/statTypeRepository');

exports.all = async (req, res) => {
  try {
    const statType = await repository.all();
    res.send(statType.Items);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
};

exports.set = async (req, res) => {
  try {
    await repository.setStatTypes(req.body);
    res.sendStatus(200);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
}