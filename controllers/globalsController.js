const repository = require('../repositories/globalsRepository');

exports.all = async (req, res) => {
  try {
    const globals = await repository.all();
    res.send(globals.Items);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
};

exports.set = async (req, res) => {
  try {
    await repository.setGlobals(req.body);
    res.sendStatus(200);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
}