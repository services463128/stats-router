const repository = require('../repositories/statsRepository');

exports.todays = async (req, res) => {
  try {
    res.send(await repository.todays(
      parseInt(req.query.id_supplier)
    ));
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
};