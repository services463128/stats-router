const repository = require('../repositories/sellerStatsRepository');

exports.get = async (req, res) => {
  try {
    const idSupplier = parseInt(req.query.id_supplier);
    if(!idSupplier) { throw new Error('no id_supplier given')}
    const sellerStats = await repository.get(idSupplier);
    res.send(sellerStats.Items);
  } catch (error) {
    console.log(error)
    res.sendStatus(500);
  }
};