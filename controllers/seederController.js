const seeder = require('../seed/seeder');
const features = require('../seed/features.js');
const statType = require('../seed/statType.js');

exports.features = async (req, res) => {
  await seeder.run(features, process.env.TABLE_FEATURES)
  .catch(error => {
    console.log(error)
    res.sendStatus(500)
  });

  res.sendStatus(200);
}

exports.statType = async (req, res) => {
  await seeder.run(statType, process.env.TABLE_STATTYPE)
  .catch(error => {
    console.log(error)
    res.sendStatus(500)
  });

  res.sendStatus(200);
}